#!/bin/sh

set -eo pipefail

IMAGE_NAME=$1

if [ -z ${IMAGE_NAME} ]
then
    echo "Please provide a name for the image."
    echo "Ex: ./build.sh 'luadev'"
    exit -1
fi

ALPINE_VERSION='3.14.2'

LUAROCKS_VERSION='3.7.0'

LUA_VERSIONS='luajit lua5.1 lua5.2 lua5.3 lua5.4'

for version in ${LUA_VERSIONS}
do
    echo "Building ${IMAGE_NAME} Docker image for ${version}..."
    docker build . \
           --quiet \
           --tag=${IMAGE_NAME}:${version} \
           --build-arg ALPINE_VERSION=${ALPINE_VERSION} \
           --build-arg LUAROCKS_VERSION=${LUAROCKS_VERSION} \
           --build-arg LUA_VERSION=${version}
    echo "Successfully built Docker image: ${IMAGE_NAME}:${version}"
done
