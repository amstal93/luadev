# luadev Dockerfile
# Produces an image (Alpine-based) with all required facilities for Lua and LuaRocks development

ARG ALPINE_VERSION
FROM alpine:${ALPINE_VERSION}

LABEL maintainer='Jessie Hildebrandt <jessieh@jessieh.net>'

ENV PORT 80
WORKDIR /workspace

# Install required packages for working with Lua and building LuaRocks
ARG LUA_VERSION
RUN apk add --no-cache \
    bash \
    bash-completion \
    ca-certificates \
    cmake \
    gcc \
    gcompat \
    git \
    libc-dev \
    ${LUA_VERSION} \
    ${LUA_VERSION}-dev \
    make \
    openssl \
    tar \
    unzip \
    wget

# Fetch, build, and install LuaRocks
ARG LUAROCKS_VERSION
RUN wget https://luarocks.org/releases/luarocks-${LUAROCKS_VERSION}.tar.gz && \
    tar xf luarocks-${LUAROCKS_VERSION}.tar.gz && \
    cd luarocks-${LUAROCKS_VERSION} && \
    ./configure && make && make install && \
    cd .. && \
    rm -rf luarocks-${LUAROCKS_VERSION}.tar.gz luarocks-${LUAROCKS_VERSION}/

# Set a nice bash prompt with a nifty whale (because the Docker logo is whale, right? It's funny)
RUN echo "export PS1='\n\[\e[0;93m\]🐳 luadev:${LUA_VERSION} \[\e[0m\]\h\[\e[0m\] \[\e[0;90m\]\w\n\[\e[0m\]\$ \[\e[0m\]'" >> ~/.bashrc

# If no command is provided to `docker run`, drop into a bash prompt by default
CMD ["bash"]

ENTRYPOINT ["/bin/bash", "-l", "-c"]
